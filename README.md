##Smsgateway android application
App is built with [Phonegap](http://phonegap.com) platform. [React.js](http://facebook.github.io/react/) and [Flux](https://facebook.github.io/flux/docs/overview.html#content) are used to create ui.
###Folder stucture
Project structure is a standart phonegap generated structure
```
www/                     --> application ui source code
   components/           --> React.js components
   actions/              --> Flux actions
   stores/               --> Flux stores
   js/
	  index.js           --> main application starter script.
   constants/            --> Flux constants
   services/
       apiclient.coffee  --> Backed api communication module
   
webpack.config.js        --> Webpack configuration file for development
webpack.production.config.js --> Webpack config file for production
```
###Plugins used by app
Sending sms messages
https://github.com/cordova-sms/cordova-sms-plugin

Receiving sms messages
https://github.com/floatinghotpot/cordova-plugin-sms

Enable background mode
https://github.com/katzer/cordova-plugin-background-mode

###Running app in development
1. Clone the repo
2. open project folder
3. `npm install`
4. `npm start` -- start serving the app
5. open new terminal window
6. open project folder
7. `npm run watch` start watching files for changes. Compile project with webpack on file change.
8. open [Phonegap developer](https://play.google.com/store/apps/details?id=com.adobe.phonegap.app) app on your android device
9. Point it to application address in the network

###Build apk file
`npm run build`

path to the apk file will be output to the console. 