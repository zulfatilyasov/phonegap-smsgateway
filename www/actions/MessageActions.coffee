AppDispatcher = require '../AppDispatcher.coffee'
UserContstants = require '../constants/UserConstants.js'
MessageContstants = require '../constants/MessageConstants.js'
apiClient  = require '../services/apiclient.coffee'
config = require '../config.coffee'
loginDelay = 1000
isSending = false

startSendingDev = ->
    if isSending
        return
    createSocketConnection()
    socket.on 'send-message', (message) ->
        return unless message
        message.outcoming = true
        message.status = 'sent'
        AppDispatcher.handleServerAction
            actionType: MessageContstants.RECEIVED_FROM_SERVER
            message: message
        MessageActions.updateUserMessageStatus(message.userId, message.id, 'sent')

    isSending = true

stopSendingDev = ->
    isSending = false
    socket.removeAllListeners('send-message')
    socket.disconnect()

socket = {}
createSocketConnection = ->
    token = localStorage.getItem('sg-token')
    model = device.model
    query = "accessToken=#{token}&origin=mobile&device=#{model}"
    options = 
        query:query
        forceNew:true
    socket = io.connect config.host, options

sendMessageToServer = (e) ->
    smsMessage = e.data

    message =
        body: smsMessage.body
        address: smsMessage.address
        status: 'received'
        origin: 'mobile'
        incoming: true

    apiClient.sendMessage(message)
        .then (resp) ->
            AppDispatcher.handleServerAction
                actionType: MessageContstants.RECEIVED_FROM_PHONE
                message: message
            , (error) ->
                console.log 'error posting incoming message'


MessageActions = 
    updateUserMessageStatus: (userId, messageId, status) ->
        apiClient.updateUserMessageStatus(userId, messageId, status)

    stopSending: ->
        socket.removeAllListeners('send-message')
        socket.disconnect()
        smsWatcher = window.SMS
        if(smsWatcher)
            smsWatcher.stopWatch()
        document.removeEventListener 'onSMSArrive', sendMessageToServer
        isSending = false
        cordova.plugins.backgroundMode.disable()

    getAddressList:()->
      apiClient.getAddressList()
        .then (resp) ->
          console.log 'received addresses'
          addresses = resp.body.addresses
          AppDispatcher.handleViewAction
            actionType: MessageContstants.GET_ADDRESSLIST_SUCCESS
            addresses: addresses

    sendToMultipleContacts:(message, contacts, groupIds)->
        apiClient.sendToMultipleContacts(message, contacts, groupIds)
             .then (resp) ->
                savedMessages = resp.body.messages
                AppDispatcher.handleViewAction
                    actionType: MessageContstants.SEND_MULTIPLE_SUCCESS
                    messages: savedMessages
            , (err) ->
                console.log err
                AppDispatcher.handleViewAction
                    actionType: MessageContstants.SEND_MULTIPLE_FAIL
                    error: err
                    message:message

        AppDispatcher.handleViewAction
            actionType: MessageContstants.SEND


    startSending: ->
        if isSending
            return
        cordova.plugins.backgroundMode.setDefaults text: 'Watching for sms messages.'
        cordova.plugins.backgroundMode.enable()

        smsWatcher = window.SMS
        if smsWatcher
            smsWatcher.startWatch ->
                console.log 'started watching'
            , ->
                alert 'failed to start watching for sms messages'

        document.addEventListener 'onSMSArrive', sendMessageToServer

        createSocketConnection()
        socket.on 'send-message', (message) ->
            return unless message
            options =
                replaceLineBreaks: false
                android:
                    intent: ''

            window.sms.send message.address, message.body, options
            , ->
                message.outcoming = true
                message.status = 'sent'
                MessageActions.updateUserMessageStatus(message.userId, message.id, 'sent')
                AppDispatcher.handleServerAction
                    actionType: MessageContstants.RECEIVED_FROM_SERVER
                    message: message

            , (err) ->
                console.log err
                MessageActions.updateUserMessageStatus(message.userId, message.id, 'failed')
                message.status = 'failed'
                AppDispatcher.handleServerAction
                    actionType: MessageContstants.RECEIVED_FROM_PHONE
                    message: message

        isSending = true


if process.env.NODE_ENV == "development"
    MessageActions.startSending =  startSendingDev
    MessageActions.stopSending = stopSendingDev
    
module.exports = MessageActions
