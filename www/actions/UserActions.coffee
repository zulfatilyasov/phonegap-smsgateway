AppDispatcher = require '../AppDispatcher.coffee'
UserContstants = require '../constants/UserConstants.js'
apiClient  = require '../services/apiclient.coffee'

loginDelay = 0;
UserActions = 
    register: (registrationData) ->
        apiClient.register registrationData
            .then (resp) ->
                data = resp.body
                AppDispatcher.handleServerAction
                    actionType: UserContstants.REGISTER_SUCCESS
                    data: data

            , (err) ->
                error = err.response.body.error
                AppDispatcher.handleServerAction
                    actionType: UserContstants.REGISTER_FAIL
                    error: error


        AppDispatcher.handleViewAction
            actionType: UserContstants.REGISTER
            registrationData: registrationData

    login: (creds) ->
        apiClient.login(creds.email, creds.password)
            .then (resp) ->
                data = resp.body
                AppDispatcher.handleServerAction
                    actionType: UserContstants.LOG_IN_SUCCESS
                    data: data

            , (err) ->
                error = err.response.body.error;
                AppDispatcher.handleServerAction
                    actionType: UserContstants.LOG_IN_FAIL
                    error: error

        AppDispatcher.handleViewAction
            actionType: UserContstants.LOG_IN
            creds: creds

    logout: -> 
        AppDispatcher.handleViewAction
            actionType: UserContstants.LOG_OUT

    requestResetPassword: (email) ->
        apiClient.requestResetPassword(email)
            .then (resp) ->
                data = resp.body
                AppDispatcher.handleServerAction
                    actionType: UserContstants.RESET_PASSWORD_SUCCESS
                    data: data

            , (err) ->
                error = err.response.body.error;
                AppDispatcher.handleServerAction
                    actionType: UserContstants.RESET_PASSWORD_FAIL
                    error: error

        AppDispatcher.handleViewAction
            actionType: UserContstants.RESET_PASSWORD
            email: email

    resetPassword: (accessToken, password, confirmation) ->
        apiClient.resetPassword accessToken, password, confirmation
            .then (resp) ->
                data = resp.body
                AppDispatcher.handleServerAction
                    actionType: UserContstants.SET_PASSWORD_SUCCESS
                    data: data

            , (err) ->
                AppDispatcher.handleServerAction
                    actionType: UserContstants.SET_PASSWORD_FAIL
                    error: err

        AppDispatcher.handleViewAction
            actionType: UserContstants.SET_PASSWORD



module.exports = UserActions;
