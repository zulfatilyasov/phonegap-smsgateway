devHost = 'http://192.168.0.4:3200'

config = 
  host: 'http://www.textyzy.com'
  
if process.env.NODE_ENV == "development"
  config.host = devHost

module.exports = config