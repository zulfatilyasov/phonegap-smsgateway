BaseStore = require './BaseStore.coffee';
messageConstants = require '../constants/MessageConstants.js';

_saving = false
_messages = []
_addressList = []

isPhone = (value) ->
    digits = value.match(/\d/g).length
    value.length < 15 and digits > 6 and digits < 12 and not isEmail(value)

isEmail = (value) ->
    re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    re.test(value)

class MessageStore extends BaseStore
    constructor:(actions) ->
        super(actions)

    inProgress: ->
        _saving

    getMessages: ->
        _messages

    getAddressList:->
        _addressList

    stripContacts:(addressList, userId) ->
        newContacts= _(addressList)
            .filter (c) ->
                c.value is c.label and not c.isContact and (isPhone(c.value) or isEmail(c.value))
            .map (c) ->
                name: c.label
                userId:userId
                phone:if isPhone(c.value) then c.value else null
                email:if isEmail(c.value) then c.value else null
                id:null
            .value()

        contacts = _.filter addressList, (c) -> c.isContact or isPhone(c.value) or isEmail(c.value)
        contacts = _.map contacts, (c) ->
            name: c.label
            phone:if isPhone(c.value) then c.value else null
            email:if isEmail(c.value) then c.value else null
            id:c.id

        groupIds =
            _(addressList)
                .filter isGroup: true
                .map (g) ->
                    g.value
                .value()

        contacts: contacts.concat(newContacts)
        groupIds: groupIds


actions = {}

actions[messageConstants.SEND] = (action) ->
    _saving = true
    storeInstance.emitChange()

actions[messageConstants.SEND_MULTIPLE_SUCCESS] = (action) ->
    _saving = false
    storeInstance.emitChange()

actions[messageConstants.SEND_MULTIPLE_FAIL] = (action) ->
    _saving = false
    alert('Failed to send messages');
    storeInstance.emitChange()

actions[messageConstants.RECEIVED_FROM_SERVER] = (action) ->
    _messages.unshift(action.message);
    storeInstance.emitChange()

actions[messageConstants.RECEIVED_FROM_PHONE] = (action) ->
    _messages.unshift(action.message);
    storeInstance.emitChange()

actions[messageConstants.GET_ADDRESSLIST_SUCCESS] = (action) ->
    console.log 'store addresses'
    _addressList = action.addresses
    storeInstance.emitChange()


storeInstance = new MessageStore(actions)

module.exports = storeInstance
