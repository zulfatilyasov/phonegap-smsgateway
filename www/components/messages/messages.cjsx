React = require('react')
MessageItem = require './message-item.cjsx'
userActions = require '../../actions/UserActions.coffee'
messageActions = require '../../actions/MessageActions.coffee'
messageStore = require '../../stores/MessageStore.coffee'
userStore = require '../../stores/UserStore.coffee'
config = require '../../config.coffee'
{FloatingActionButton, RaisedButton, TextField} = require 'material-ui'
Select = require 'react-select'

Messages = React.createClass

  getInitialState: ->
    messages: messageStore.getMessages()
    saving : false
    showForm: false
    addressList:messageStore.getAddressList()
  
  componentDidMount: ->
    messageActions.startSending()
    messageActions.getAddressList()
    messageStore.addChangeListener @onChange

  componentWillUnmount: ->
    messageActions.stopSending()
    messageStore.removeChangeListener @onChange

  onChange: ->
    state = @getState()
    if messageStore.inProgress()
      state.readyToClose = true
    else
      if @state.readyToClose
          state.showForm = false
      state.readyToClose = false

    @setState state

  getState: ->
    messages: messageStore.getMessages()
    saving :  messageStore.inProgress()
    addressList: messageStore.getAddressList()

  addressChanged:(e,values)->
    @selectedAddresses = values

  handleBodyChange:(e)->
    @body = e.target.value

  handleAddMessageClick:(e)->
    @setState
      showForm:true

  handleSave:(e) ->
    e.preventDefault()
    self = @
    userId = userStore.userId()
    recipients = messageStore.stripContacts(@selectedAddresses, userId)

    message = 
      body: self.body
      userId: userId
      status: 'queued'
      outcoming: true
      handler: 'phone'
      origin: 'web'

    messageActions.sendToMultipleContacts message, recipients.contacts, recipients.groupIds

  handleCancelSave:->
    @setState
      showForm:false

  render: ->
    primaryButtonLabel = if @state.saving then 'Sending..' else 'Send'
    className = if @state.saving then 'saving' else ''
    saveButtonClass = 'button saveButton ' + className
    formClasses = if @state.showForm then 'message-form open' else 'message-form'

    <div className = "messages">
      <div className="top-bar">
      </div>
      <div className="header">
        <div className="title">
          Message stream
        </div>
        <div className="actions">
          <a onClick={@handleLogoutClick} href=""><i className="icon icon-exit-to-app"></i></a>
        </div>
      </div>

      <div className={formClasses}>
        <Select
          multi={true}
          placeholder="Address"
          allowCreate={true}
          options={@state.addressList}
          onChange={@addressChanged} />

        <div className="body-input-wrap">
          <TextField
            className="body-input"
            multi={true}
            hintText="Message body"
            errorText=''
            onChange={@handleBodyChange}
            multiLine={true}/>
        </div>

        <div className="actions">
          <RaisedButton
            className="button cancel"
            onClick={@handleCancelSave}
            linkButton={true}
            label="cancel" >
          </RaisedButton>
          <RaisedButton
            className={saveButtonClass}
            primary={true}
            disabled={@state.saving}
            onClick={@handleSave}
            linkButton={true}
            label={primaryButtonLabel} >
          </RaisedButton>
        </div>
      </div>
      <FloatingActionButton onClick={@handleAddMessageClick} className="add-message" iconClassName="icon icon-plus" />
      {
        if @state.messages.length
          <div className="list">
            {
              @state.messages.map (msg) ->
                <MessageItem key={msg._id} {...msg}/>
            }
          </div>
      }
      {
        if not @state.messages.length
          <div className="no-messages">No messages</div>
      }
    </div>

  handleLogoutClick: (e) ->
    e.preventDefault()
    userActions.logout()

module.exports = Messages