React = require('react')
{Paper} = require 'material-ui'
classBuilder = require 'classnames'

MessageItem = React.createClass

  render: ->
    iconClass = classBuilder 
      'icon':true
      'icon-arrow-down-right2 incoming': @props.status is 'received'
      'icon-arrow-up-left2 outcoming': @props.status is 'sent'
      'icon-sms-failed fail': @props.status is 'failed'

    <div className="message-item">
      <Paper zDepth={1}>
        <div className="padded">
          <div className="message-inner">
            <div className="message-icon">
              <i className={iconClass}></i>
            </div>
            <div className="message-content">
              <div className="phone">{@props.address}</div>
              <div className="message-text">{@props.body}</div>
            </div>
          </div>
        </div>
      </Paper>
    </div>

module.exports = MessageItem