React = require 'react'
LoginPage = require '../login/login-page.cjsx'
Messages = require '../messages/messages.cjsx'
styles = require './app.styl'
userStore = require '../../stores/UserStore.coffee'

App = React.createClass
  getInitialState: ->
    isAuthenticated: userStore.isAuthenticated()

  componentDidMount: ->
    userStore.addChangeListener @_onChange

  componentWillUnmount: ->
    userStore.removeChangeListener @_onChange

  _onChange: ->
    @setState(@getState())

  getState:->
    isAuthenticated: userStore.isAuthenticated()
  
  render: ->
    if @state.isAuthenticated
      <Messages />
    else
      <LoginPage />


module.exports = App