React = require 'react'
{TextField, RaisedButton, Paper} = require 'material-ui'
userActions = require '../../actions/UserActions.coffee'
messageActions = require '../../actions/MessageActions.coffee'
userStore = require '../../stores/UserStore.coffee'
style = require './login.styl'
classBuilder = require 'classnames'

Login = React.createClass
  getInitialState: ->
    authError: userStore.AuthError()

  componentDidMount: ->
    userStore.addChangeListener @_onChange

  componentWillUnmount: ->
    userStore.removeChangeListener @_onChange
    messageActions.startSending()

  getState: ->
    authError: userStore.AuthError()

  _onChange: ->
    @setState @getState()
  
  render: ->
    errorClass = classBuilder
      autherror: true,
      hide: not @state.authError.hasError

    <div className="login-wrap">
      <div className="header">
        <div className="title">
          Log in
        </div>
      </div>
      <div className="login-content">
        <h1 className="login-header">SMS Gateway</h1>
        <form className="login-form">
            <Paper zDepth={1}>
                <div className="padded">
                  <TextField
                      className="email-input"
                      hintText="Email"
                      errorText=''
                      onChange={@_handleEmailChange}
                      ref="emailInput"/>

                  <TextField
                      className="password-input"
                      hintText="Password"
                      type="password"
                      errorText=''
                      onChange={@_handlePasswordChange}
                      ref="emailInput"/>

                  <div className="button-wrap">
                    <div className={errorClass}>{@state.authError.message}</div>

                    <RaisedButton
                        primary={true}
                        onClick={@_handleLogin}
                        className="loginButton"
                        label='Login'/>

                  </div>
              </div>
          </Paper>
        </form>
      </div>
    </div>

  _handleLogin: (e) ->
    e.preventDefault()
    if navigator.connection.type is Connection.NONE
      @setState 
        authError:
            hasError: true
            message: 'No internet connection'
    else
      userActions.login
        email:@email
        password:@password

  _handleEmailChange: (e) ->
    @email = e.target.value

  _handleEmailBlur: ->
    console.log 'email blur'

  _handlePasswordChange: (e) ->
    @password = e.target.value

  _handlePasswordBlur: ->
    console.log 'password blur'

module.exports = Login