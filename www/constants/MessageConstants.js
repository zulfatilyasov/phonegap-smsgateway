var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
    RECEIVED_FROM_SERVER: null,
    RECEIVED_FROM_PHONE: null,
    SENT_TO_RECEPIENT: null,
    SENT_TO_SERVER: null,
    GET_ADDRESSLIST_SUCCESS: null,
    SEND_MULTIPLE_SUCCESS: null,
    SEND_MULTIPLE_FAIL: null,
    SEND: null
});
